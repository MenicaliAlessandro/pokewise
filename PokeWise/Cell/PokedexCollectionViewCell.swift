//
//  PokedexCollectionViewCell.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 16/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import UIKit

class PokedexCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pokemonImage: UIImageView!
    
    @IBOutlet weak var lblPokemonName: UILabel!
    
    
}
