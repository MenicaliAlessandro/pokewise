//
//  Pokemon.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 14/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import Foundation
import RealmSwift

class Pokemon: Object{

    @objc dynamic var _id = 0
    @objc dynamic var name: String?
    @objc dynamic var image: String?
    @objc dynamic var type: String?
    var stats = List<PokemonStats>()
    @objc dynamic var height = 0
    @objc dynamic var weight = 0
    
    override static func primaryKey() -> String? {
           return "_id"
       }
}

class PokemonStats: Object{
    @objc dynamic var stat: PokemonStat? = nil
    @objc dynamic var base_stat = 0
    
}

class PokemonStat : Object{
    @objc dynamic var name: String?
}

