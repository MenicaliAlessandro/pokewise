//
//  ApiRequest.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 14/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import Foundation
import PokemonAPI

struct ApiRequest {
    
    static var pokemonAPI = PokemonAPI()
    
    
    
    static func getPokemonsList(completion: @escaping([PKMPokemon]) -> Void){
       
        var arrayPokemon = [PKMPokemon]()
        
        // Page: 0
        pokemonAPI.pokemonService.fetchPokemonList(paginationState: .initial(pageLimit: 500)) {
            (result) in
            switch result{
            case .success(let pkmPagedObject):
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "page0"), object: nil)
                print("Page: \(pkmPagedObject.currentPage)") // Page: 0
                for resource in pkmPagedObject.results!{
                    pokemonAPI.resourceService.fetch(resource) {
                        (result) in
                        switch result{
                        case .success(let pokemon):
                            arrayPokemon.append(pokemon)
//                            print("\(pokemon.id)" + " -> " + "\(pokemon.name)")
                        case .failure(let error):
                            print(error.localizedDescription)
                        }
                    }
                    
                }
                
                // Page: 1
                pokemonAPI.pokemonService.fetchPokemonList(paginationState: .continuing(pkmPagedObject, .next)){
                    (result) in
                    
                    switch result {
                    case .success(let pkmPagedObjectNext):
//                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "page1"), object: nil)
                        print("Page: \(pkmPagedObjectNext.currentPage)") // Page: 1
                        
                        for resource in pkmPagedObjectNext.results!{
                            pokemonAPI.resourceService.fetch(resource) {
                                (result) in
                                switch result{
                                case .success(let pokemon):
                                    arrayPokemon.append(pokemon)
//                                    print("\(pokemon.id)" + " -> " + "\(pokemon.name)")
                                    if arrayPokemon.count == pkmPagedObject.count{
                                        completion(arrayPokemon)
                                    }
                                    
                                case .failure(let error):
                                    print(error.localizedDescription)
                                }
                            }
                            
                        }
                        
                        
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
    }
    
    static func getPokemonCount(completion:@escaping(Int) -> Void){
        pokemonAPI.pokemonService.fetchPokemonList { (result) in
            switch result{
            case .success(let pkmPagedObject):
                completion(pkmPagedObject.count!)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    static func getPokemonStats(id: Int, completion: @escaping([PokemonStats]) -> Void){
       
        let myGroup = DispatchGroup()
        
       
        var pokemonStats = [PokemonStats]()
        
        pokemonAPI.pokemonService.fetchPokemon(id) { (
            result) in
            switch result{
            case .success(let pokemon):
                for stat in pokemon.stats!{
                    myGroup.enter()
                    
                    pokemonAPI.resourceService.fetch(stat.stat!) {
                        (result) in
                        switch result{
                        case .success(let pkmStat):
                            let pkmonStat = PokemonStat()
                            let pkmonStats = PokemonStats()
                            pkmonStats.stat = pkmonStat
                            pkmonStats.stat?.name = pkmStat.name ?? ""
                            pkmonStats.base_stat = (stat.baseStat!)
                            pokemonStats.append(pkmonStats)
                            
                            if pokemonStats.count == pokemon.stats?.count{
                                completion(pokemonStats)
                            }
                           myGroup.leave()
                        case .failure(let error):
                            print(error.localizedDescription)
                        }
                        
                    }
                    
                    
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        myGroup.notify(queue: .main) {
                   
               }
        
    }
    
    
    static func getPokemonTypes(id: Int, completion: @escaping(String) -> Void){
       
        var typesArray = [String]()
        var typeString = ""
        let myGroup = DispatchGroup()
        
        pokemonAPI.pokemonService.fetchPokemon(id) { (
            result) in
            switch result{
            case .success(let pokemon):
                for typeObj in pokemon.types!{
                    myGroup.enter()
                    pokemonAPI.resourceService.fetch(typeObj.type!) {
                        (result) in
                        switch result{
                        case .success(let pkmType):
                            
                            typesArray.append(pkmType.name!)
                            
                            if typesArray.count == pokemon.types?.count{
                                for type in typesArray{
                                    typeString += typeString + " - " + type
                                }
                                completion(pkmType.name!)
                            }
                           myGroup.leave()
                        case .failure(let error):
                            print(error.localizedDescription)
                        }
                        
                    }
                    
                    
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        myGroup.notify(queue: .main) {
                   
               }
        
    }
    
    
}
