//
//  SessionManager.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 14/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import Foundation
import RealmSwift


class SessionManager{
    
    static let shared = SessionManager()
    
    var realm = try! Realm()
    
    private init(){
    }
    
    func getRealmPath() -> String{
        return Realm.Configuration.defaultConfiguration.fileURL!.absoluteString
    }
    
    
    
    
}
