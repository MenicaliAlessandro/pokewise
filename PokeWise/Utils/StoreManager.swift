//
//  StoreManager.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 16/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import Foundation
import UIKit

struct StoreManager {
    
    public static var folderPokemonSprites = "PokemonSprites";
    
    private static func createDir(folder:String)->String{
        
        let fileManager = FileManager.default
        if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
            let filePath =  tDocumentDirectory.appendingPathComponent(folder)
            
            if !fileManager.fileExists(atPath: filePath.path) {
                do {
                    try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    NSLog("Couldn't create document directory")
                    return ""
                }
            }
            //NSLog("Document directory is \(filePath)")
            return filePath.absoluteString
        }
        
        return ""
    }
    
    
    public static func savePokemonSprite(image : UIImage, pokemon : Pokemon){
        
        let folder = createDir(folder: folderPokemonSprites)
        
        if folder.count>0{
            let fileManager = FileManager.default
            let filename = (folder + "_" + pokemon.name! + "_" + String(pokemon._id) + ".png")
            
            let imageData = image.fixOrientation().jpegData(compressionQuality: 0.5)
            //fileManager.createFile(atPath: filename, contents: imageData, attributes: nil)
            try? FileManager.default.removeItem(atPath: filename)
            let url = URL.init(string: filename)
            try? imageData?.write(to: url!)
        }
    }
    
    public static func getPokemonSprite(namePokemon: String, idPokemon: String)->String?{
        
        let folder = createDir(folder: folderPokemonSprites)
        
        if folder.count>0{
            
            
            let fileManager = FileManager.default
            if let dirContents = fileManager.enumerator(at: URL.init(string: folder)!, includingPropertiesForKeys: nil) {
                let imagesUrl : NSMutableArray = []
                for case let url as URL in dirContents {
                    let sdtringPath = url.path
                    if (sdtringPath.contains(namePokemon + "_" + idPokemon)){
                        imagesUrl.add(URL.init(string: sdtringPath))
                        //images.add(sdtringPath)
                        return url.path
                    }
                }
                
                
            }
        }
        
        
        return nil
    }
}

extension UIImage {
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }
}
