//
//  RoundedButton.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 16/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable open var round : CGFloat = 8{
        didSet{
            layoutSubviews()
        }
    }
    
    @IBInspectable open var circle : Bool = false{
        didSet{
            layoutSubviews()
        }
    }
    
    override open var isEnabled: Bool{
        didSet{
            self.alpha = isEnabled ? 1:0.3
        }
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = round
        self.alpha = isEnabled ? 1:0.4
        if circle{
            self.layer.cornerRadius = self.layer.frame.size.height/2
        }
        super.layoutSubviews()
        
    }

}
