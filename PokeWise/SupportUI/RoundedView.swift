//
//  RoundedView.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 16/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import UIKit

class RoundedView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var round : CGFloat = 8{
        didSet{
            layoutSubviews()
        }
    }
    
    @IBInspectable var circle : Bool = true{
        didSet{
            layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = round
        if circle{
            self.layer.cornerRadius = self.layer.frame.size.height/2
        }
        super.layoutSubviews()
        
    }

}
