//
//  PokedexViewController.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 16/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import UIKit
import RealmSwift

class PokedexViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
   
    
    
    //Vars:
    var pokedex: Results<Pokemon>!
    var refreshControl: UIRefreshControl!
    
    //IBOutlset:
    @IBOutlet weak var imageBg: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    //IBactions:
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let vEffect = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        vEffect.frame = imageBg.bounds
        imageBg.addSubview(vEffect)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
               refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
               collectionView.addSubview(refreshControl)
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        let pokemonCount = SessionManager.shared.realm.objects(Pokemon.self).count
        return pokedex.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pokedexCell", for: indexPath) as! PokedexCollectionViewCell
        
        let pokemon : Pokemon = pokedex[indexPath.row]
        
        cell.lblPokemonName.text = pokemon.name
        
         // image

        let imagePath = StoreManager.getPokemonSprite(namePokemon: pokemon.name!, idPokemon: String(pokemon._id))
                
        //        print(imagePath)
                if imagePath != nil {
                    let image = UIImage.init(contentsOfFile: imagePath!)
                    cell.pokemonImage.image = image
                    cell.pokemonImage.layer.cornerRadius = cell.pokemonImage.frame.height/2
                   
                    cell.pokemonImage.layer.shouldRasterize = true
                    cell.pokemonImage.clipsToBounds = true
                }
                else{
                    cell.pokemonImage.image = nil
                    
                }
        
        return cell
       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let detailPokemonVC = self.storyboard?.instantiateViewController(withIdentifier: "detailPokemonVC") as! DetailPokemonViewController
        
        let pokemon: Pokemon = pokedex[indexPath.row]
        
        detailPokemonVC.pokemon = pokemon
        
        
        self.navigationController?.pushViewController(detailPokemonVC, animated: true)
    }
    
    @objc func refresh(_ sender: Any) {
           loadData()
           refreshControl.endRefreshing()
       }
    
    
    func loadData(){
       
            pokedex =  SessionManager.shared.realm.objects(Pokemon.self).sorted(byKeyPath: "_id", ascending: true)
            
            collectionView.reloadData()
    }
   
}
