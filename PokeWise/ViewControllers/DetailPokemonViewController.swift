//
//  DetailPokemonViewController.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 16/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import UIKit

class DetailPokemonViewController: UIViewController {
    
    // var:
    var pokemon: Pokemon?
    
    //IBoutlets:
    @IBOutlet weak var imagePokemon: UIImageView!
    @IBOutlet weak var lblPokemonName: UILabel!
    @IBOutlet weak var lblPokemonType: UILabel!
    
    @IBOutlet weak var lblHeightTitle: UILabel!
    @IBOutlet weak var lblHeightValue: UILabel!
    
    @IBOutlet weak var lblWeightTitle: UILabel!
    @IBOutlet weak var lblWeightValue: UILabel!
    
    
    @IBOutlet weak var lblHpTitle: UILabel!
    @IBOutlet weak var lblHpValue: UILabel!
    
    @IBOutlet weak var lblSpeedTitle: UILabel!
    @IBOutlet weak var lblSpeedValue: UILabel!
    
    @IBOutlet weak var lblAttackTitle: UILabel!
    @IBOutlet weak var lblAttackValue: UILabel!
    
    @IBOutlet weak var lblDefTitle: UILabel!
    @IBOutlet weak var lblDefValue: UILabel!
    
    @IBOutlet weak var lblSpecialAttackTitle: UILabel!
    @IBOutlet weak var lblSpecialAttackValue: UILabel!
    
    @IBOutlet weak var lblSpecialDefTitle: UILabel!
    @IBOutlet weak var lblSpecialDefValue: UILabel!
    
    
    //IBactions:
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //image
        let imagePath = StoreManager.getPokemonSprite(namePokemon: pokemon!.name!, idPokemon: String(pokemon!._id))
        
        if imagePath != nil {
            let image = UIImage.init(contentsOfFile: imagePath!)
            imagePokemon.image = image
            imagePokemon.layer.cornerRadius = imagePokemon.frame.height/2
            
            imagePokemon.layer.shouldRasterize = true
            imagePokemon.clipsToBounds = true
        }else{
            
        }
        
        if pokemon != nil{
            
            lblPokemonName.text = "#" + String(pokemon!._id) + " " + (pokemon?.name)!
            
            lblPokemonType.text = "( " + (pokemon?.type)! + " )"
            
            lblHeightTitle.text = "Height"
            if pokemon?.height != nil{
                lblHeightValue.text = String(pokemon!.height)
            }
            
            if pokemon?.weight != nil{
                lblWeightTitle.text = "Weight"
                lblWeightValue.text = String(pokemon!.weight)
            }
            
            
            for stat in pokemon!.stats{
                
                if stat.stat?.name == "hp"{
                    lblHpTitle.text = "HP"
                    lblHpValue.text = String(stat.base_stat)
                }
                
               if stat.stat?.name == "attack"{
                   lblAttackTitle.text = "ATK"
                   lblAttackValue.text = String(stat.base_stat)
               }
                
                if stat.stat?.name == "defense"{
                    lblDefTitle.text = "DEF"
                    lblDefValue.text = String(stat.base_stat)
                }
                
                if stat.stat?.name == "special-attack"{
                    lblSpecialAttackTitle.text = "SPEC-ATK"
                    lblSpecialAttackValue.text = String(stat.base_stat)
                }
                
                if stat.stat?.name == "special-defense"{
                    lblSpecialDefTitle.text = "SPEC-DEF"
                    lblSpecialDefValue.text = String(stat.base_stat)
                }
                
                if stat.stat?.name == "speed"{
                    lblSpeedTitle.text = "SPEED"
                    lblSpeedValue.text = String(stat.base_stat)
                }
                
                
                
                
            }
            
        }
        
    }
    
    
    
    
}
