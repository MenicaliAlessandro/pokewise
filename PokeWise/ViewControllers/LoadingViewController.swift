//
//  LoadingViewController.swift
//  PokeWise
//
//  Created by Alessandro Menicali on 13/07/2020.
//  Copyright © 2020 Alessandro Menicali. All rights reserved.
//

import UIKit
import PokemonAPI
import RealmSwift

class LoadingViewController: UIViewController {
    
    // Vars:
    var pokemonsData: [PKMPokemon]?
    var pokemonToUpdate: [Pokemon]?
    var pokedex: Results<Pokemon>!
    var pkmStoredCnt = 0
    let progress = Progress(totalUnitCount: 10)
    
    
    // IBOutlets:
    @IBOutlet weak var lblProgressBar: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var btnNext: RoundedButton!
    
    // IBActions:
    @IBAction func btnNextTapped(_ sender: RoundedButton) {
        
        let navigationVC = self.storyboard?.instantiateViewController(withIdentifier: "mainNC") as! UINavigationController
        
        self.present(navigationVC, animated: true, completion: nil)
        
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressBar.progress = 0.0
        progress.completedUnitCount = 0
        progressBar.setProgress(Float(self.progress.fractionCompleted), animated: true)
        
        let realmPath = SessionManager.shared.getRealmPath()
        print(realmPath)
        
        lblProgressBar.text = "Check Internet Connection..."
        
        let isEmpty = SessionManager.shared.realm.isEmpty
        
        if isEmpty{
            // do nothing
        }
        else{
            btnNext.isHidden = false
        }
        
        btnNext.isHidden = true
        
        checkRechability()
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.firstProgressBar), name: NSNotification.Name(rawValue: "page0"), object: nil)
        //
        //         NotificationCenter.default.addObserver(self, selector: #selector(self.secondProgressBar), name: NSNotification.Name(rawValue: "page1"), object: nil)
        //
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.finalProgressBar), name: NSNotification.Name(rawValue: "requestCompleted"), object: nil)
    }
    
    func checkRechability(){
        if Reachability.isConnectedToNetwork(){
            lblProgressBar.text = "Retrieving data..."
            loadPokemonPartialData { (completed) in
                if completed{
                    self.progress.completedUnitCount = 4
                    self.progressBar.setProgress(Float(self.progress.fractionCompleted), animated: true)
                    // func stats
                    self.loadPokemonStats {
                        (completed) in
                        if completed{
                            DispatchQueue.main.async {
                                autoreleasepool{
                                    self.progress.completedUnitCount = 8
                                    self.progressBar.setProgress(Float(self.progress.fractionCompleted), animated: true)
                                }
                            }
                            self.updatePokemonStats {
                                (completed) in
                                if completed{
                                    self.loadPokemonType {
                                        (completed) in
                                        if completed{
                                            self.updatePokemonTypes()
                                        }
                                    }
                                }
                                
                            }
                            
                            
                            self.loadPokemonSprites()
                        }
                    }
                }
            }
        }else{
            lblProgressBar.text = "Internet Connection not available"
        }
    }
    
    func checkRealmData(pkmEffectiveNumber: Int, pkmStoredCount: Int) -> Bool{
        
        
        let isEmpty = SessionManager.shared.realm.isEmpty
        
        if !isEmpty{
            if pkmStoredCount == pkmEffectiveNumber{
                return true
            }
            else{
                return false
            }
        }
        else{
            return false
        }
        
    }
    
    func loadPokemonPartialData(completion:@escaping(Bool) -> Void){
        
        // check internet connection
        
        pkmStoredCnt = SessionManager.shared.realm.objects(Pokemon.self).count
        
        ApiRequest.getPokemonCount {
            (effectiveCount) in
            let equal =  self.checkRealmData(pkmEffectiveNumber: effectiveCount, pkmStoredCount: self.pkmStoredCnt)
            
            if equal {
                DispatchQueue.main.async {
                    autoreleasepool{
                        self.lblProgressBar.text = ""
                        self.progress.completedUnitCount = 10
                        self.progressBar.setProgress(Float(self.progress.fractionCompleted), animated: true)
                    }
                }
                self.btnNext.isHidden = false
                return
            }else{
                
                DispatchQueue.main.async {
                    autoreleasepool{
                        do{
                            try SessionManager.shared.realm.write{
                                SessionManager.shared.realm.deleteAll()
                            }
                            
                        }catch let error as NSError{
                            print("Something went wrong: \(error.localizedDescription)")
                        }
                    }
                }
                
                ApiRequest.getPokemonsList(completion: {
                    (pkmsData) in
                    
                    // order data by id
                    
                    let tempData = pkmsData.sorted(by: { $0.id! < $1.id!})
                    self.pokemonsData = tempData
                    
                    // prepare pokemon object to store into realm
                    
                    DispatchQueue.main.async {
                        autoreleasepool {
                            do {
                                try SessionManager.shared.realm.write {
                                    for pokemon in self.pokemonsData!{
                                        let pkm = Pokemon()
                                        pkm._id = pokemon.id! as Int
                                        pkm.name = pokemon.name! as String
                                        pkm.image = pokemon.sprites?.frontDefault
                                        pkm.height = (pokemon.height ?? 0) as Int
                                        pkm.weight = (pokemon.weight ?? 0) as Int
                                        
                                        SessionManager.shared.realm.add(pkm)
                                        
                                        if SessionManager.shared.realm.objects(Pokemon.self).count == self.pokemonsData?.count{
                                            completion(true)
                                        }
                                    }
                                }
                            } catch let error as NSError {
                                print("Something went wrong: \(error.localizedDescription)")
                            }
                        }
                        
                    }
                    
                })
            }
        }
        
        
    }
    
    func loadPokemonStats(completion:@escaping(Bool) -> Void){
        pkmStoredCnt = SessionManager.shared.realm.objects(Pokemon.self).count
        pokemonToUpdate = [Pokemon]()
        let myGroup = DispatchGroup()
        for pkm in pokemonsData!{
            myGroup.enter()
            
            ApiRequest.getPokemonStats(id: pkm.id!) {
                (pokemonStats) in
                
                let pokemon = Pokemon()
                pokemon._id = pkm.id! as Int
                for pkmStats in pokemonStats{
                    pokemon.stats.append(pkmStats)
                }
                self.pokemonToUpdate?.append(pokemon)
                
                if self.pokemonToUpdate!.count == self.pkmStoredCnt{
                    completion(true)
                }
                myGroup.leave()
            }
        }
        
        myGroup.notify(queue: .main) {
            self.lblProgressBar.text = ""
            DispatchQueue.main.async {
                autoreleasepool{
                    self.progress.completedUnitCount = 10
                    self.progressBar.setProgress(Float(self.progress.fractionCompleted), animated: true)
                }
            }
            self.btnNext.isHidden = true
            print("Finished all requests.")
        }
        
    }
    
    func loadPokemonType(completion:@escaping(Bool) -> Void){
        DispatchQueue.main.async {
            autoreleasepool{
                self.pkmStoredCnt = SessionManager.shared.realm.objects(Pokemon.self).count
            }
        }
        pokemonToUpdate = [Pokemon]()
        let myGroup = DispatchGroup()
        
        for pkm in pokemonsData!{
            myGroup.enter()
            
            ApiRequest.getPokemonTypes(id: pkm.id!) {
                (pkmType) in
                let pokemon = Pokemon()
                pokemon._id = pkm.id! as Int
                
                pokemon.type = pkmType
                self.pokemonToUpdate?.append(pokemon)
                
                if self.pokemonToUpdate!.count == self.pkmStoredCnt{
                    completion(true)
                }
                myGroup.leave()
            }
        }
        
        myGroup.notify(queue: .main) {
            self.lblProgressBar.text = ""
            DispatchQueue.main.async {
                autoreleasepool{
                    self.progress.completedUnitCount = 10
                    self.progressBar.setProgress(Float(self.progress.fractionCompleted), animated: true)
                }
            }
            print("Finished all requests.")
        }
    }
    
    func updatePokemonStats(completion:@escaping(Bool) -> Void){
        
        // Updating pokemon by id
        DispatchQueue.main.async {
            autoreleasepool {
                do {
                    try SessionManager.shared.realm.write {
                        for pkm in self.pokemonToUpdate!{
                            
                            if let currentObject = SessionManager.shared.realm.object(ofType: Pokemon.self, forPrimaryKey: pkm._id) {
                                pkm.name = currentObject.name
                                pkm.image = currentObject.image
                                pkm.height = currentObject.height
                                pkm.weight = currentObject.weight
                            }
                            SessionManager.shared.realm.add(pkm, update: .modified)
                            
                        }
                        completion(true)
                    }
                } catch let error as NSError {
                    print("Something went wrong: \(error.localizedDescription)")
                }
            }
            
        }
    }
    
    func updatePokemonTypes(){
        
        // Updating pokemon by id
        DispatchQueue.main.async {
            autoreleasepool {
                do {
                    try SessionManager.shared.realm.write {
                        for pkm in self.pokemonToUpdate!{
                            
                            if let currentObject = SessionManager.shared.realm.object(ofType: Pokemon.self, forPrimaryKey: pkm._id) {
                                pkm.name = currentObject.name
                                pkm.image = currentObject.image
                                pkm.height = currentObject.height
                                pkm.weight = currentObject.weight
                                pkm.stats = currentObject.stats
                            }
                            SessionManager.shared.realm.add(pkm, update: .modified)
                        }
                        self.btnNext.isHidden = false
                        self.lblProgressBar.text = "Completed."
                    }
                } catch let error as NSError {
                    print("Something went wrong: \(error.localizedDescription)")
                }
            }
            
        }
    }
    
    func loadPokemonSprites(){
        
        DispatchQueue.main.async {
            autoreleasepool {
                self.pokedex =  SessionManager.shared.realm.objects(Pokemon.self).sorted(byKeyPath: "_id", ascending: true)
                for pkm in self.pokedex{
                    if pkm.image != nil{
                        let url = URL(string: pkm.image!)
                        
                        DispatchQueue.global().async {
                            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                            DispatchQueue.main.async {
                                if data != nil{
                                    let imagePKM = UIImage(data: data!)
                                    StoreManager.savePokemonSprite(image: imagePKM!, pokemon: pkm)
                                }
                                self.lblProgressBar.text = "Sprites stored..."
                            }
                        }
                    }
                }
            }
        }
        
        
    }
    
    @objc func firstProgressBar(notif: NSNotification) {
        lblProgressBar.text = "Retrieving data..."
        progress.completedUnitCount = 4
        progressBar.setProgress(Float(self.progress.fractionCompleted), animated: true)
    }
    
    @objc func secondProgressBar(notif: NSNotification) {
        lblProgressBar.text = "Retrieving data..."
        progress.completedUnitCount = 4
        progressBar.setProgress(Float(self.progress.fractionCompleted), animated: true)
    }
    
    @objc func finalProgressBar(notif: NSNotification) {
        lblProgressBar.text = "Download completed"
        progress.completedUnitCount = 4
        progressBar.setProgress(Float(self.progress.fractionCompleted), animated: true)
    }
    
}
